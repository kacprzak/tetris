#pragma once
#include "matrix.h"

enum class Direction :char { left, right, down };
class Matrix;

struct ShapeRectPos {
	int rect1PositionX;
	int rect1PositionY;
	int rect2PositionX;
	int rect2PositionY;
	int rect3PositionX;
	int rect3PositionY;
	int rect4PositionX;
	int rect4PositionY;
};

class Shape {
public:
	virtual ~Shape();
	void move(Direction direct);
	virtual void rotate() = 0;
	int getColorRed();
	int getColorGreen();
	int getColorBlue();
	virtual void initRegularShape() = 0;
	bool m_blockedShape = false;
	std::vector <SDL_Rect> m_shapePreviewRects;	

protected:
	enum Position { west, north, east, south };
	SDL_Color shapeColor;
	Matrix *matrixPtr = nullptr;
	Direction m_direct;
	Position m_shapePosition = north;
	const int m_previewStartPosX = 450;
	const int m_previewStartPosY = 50;
	ShapeRectPos m_rectPositions;
	void blockShape();
 };

// cala plansza to macierz dwuwymiarowa
// kazdy kwadrat symbolu to czesc tej macierzy 
// przy obrocie i kolizjach sprawdzamy czy dany element tablicy jest juz zajety 