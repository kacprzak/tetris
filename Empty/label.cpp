#include "label.h"
#include "stdafx.h"

Label::Label(std::string text,
						SDL_Color textColor,
						SDL_Renderer *renderer,
						std::string fontFile,
						int fontSize)
{
	m_text = text;
	m_textColor = textColor;
	m_rendererPtr = renderer;
	m_fontFile = fontFile;
	m_fontSize = fontSize;
	loadFont();
	createTextTexture();
}

Label::~Label()
{
	TTF_CloseFont(m_font);
	SDL_DestroyTexture(m_textTexture);
}

void Label::setTextPosition(int posX, int posY)
{
	m_textRect.x = posX;
	m_textRect.y = posY;
}


void Label::createTextTexture()
{
	m_textSurface = TTF_RenderText_Blended(m_font, m_text.c_str(), m_textColor);
	m_textRect.h = m_textSurface->h;
	m_textRect.w = m_textSurface->w;
	m_textTexture = SDL_CreateTextureFromSurface(m_rendererPtr, m_textSurface);
	SDL_FreeSurface(m_textSurface);
}

void Label::renderText()
{
	SDL_RenderCopy(m_rendererPtr, m_textTexture, NULL, &m_textRect);
}

void Label::changeRenderedText(std::string newText)
{
	m_text = newText;
	SDL_DestroyTexture(m_textTexture);
	m_textSurface = TTF_RenderText_Blended(m_font, m_text.c_str(), m_textColor);
	m_textRect.h = m_textSurface->h;
	m_textRect.w = m_textSurface->w;
	m_textTexture = SDL_CreateTextureFromSurface(m_rendererPtr, m_textSurface);
	SDL_FreeSurface(m_textSurface);
}

void Label::changeTextColor(SDL_Color newColor)
{
	m_textColor = newColor;
	SDL_DestroyTexture(m_textTexture);
	m_textSurface = TTF_RenderText_Blended(m_font, m_text.c_str(), m_textColor);
	m_textTexture = SDL_CreateTextureFromSurface(m_rendererPtr, m_textSurface);
	SDL_FreeSurface(m_textSurface);
}

bool Label::loadFont()
{
	m_font = TTF_OpenFont(m_fontFile.c_str(), m_fontSize);
	
	if (m_font == nullptr) 
	{
		std::cout << "Can't load font" << std::endl;
		return false;
	}
	else 
	{
		return true;
	}
}