#include "settings.h"
#include "boost\foreach.hpp"
#include "boost\filesystem.hpp"


Settings::Settings()
{
	if (boost::filesystem::exists(XML_NAME))
	{
		try
		{
			readXml(XML_NAME);
		}
		catch (std::exception &e)
		{
			std::cout << "Error: " << e.what() << "\n";
		}
	}
	else
	{
		createDefaultXml();
		readXml(XML_NAME);
	}
}

Settings::~Settings(){}

void Settings::readXml(const std::string &settingsXml)
{
	using boost::property_tree::ptree;
	boost::property_tree::xml_parser::read_xml(settingsXml, xmlTree);
	BOOST_FOREACH(ptree::value_type &v, xmlTree.get_child("settings"))
	{
		BOOST_FOREACH(ptree::value_type &u, v.second)
		{
			settingsList.insert(std::make_pair(u.first, v.second.get<std::string>(u.first)));
			std::cout << u.first << " - " << v.second.get<std::string>(u.first) << std::endl;
		}
	}
}

void Settings::createDefaultXml()
{
	using boost::property_tree::ptree;
	
	xmlTree.add("settings.windowSettings." + windWidth, 550);
	xmlTree.add("settings.windowSettings." + windHeight, 600);

	xmlTree.add("settings.keySettings." + pauseKey, "SDLK_SPACE");
	xmlTree.add("settings.keySettings." + leftKey, "SDLK_LEFT");
	xmlTree.add("settings.keySettings." + rightKey, "SDLK_RIGHT");
	xmlTree.add("settings.keySettings." + downKey, "SDLK_DOWN");
	xmlTree.add("settings.keySettings." + rotateKey, "SDLK_UP");
	
	ptree &node = xmlTree.add("settings.others." + gameSpeed, 2); //create node in order to add attribute
	node.put("<xmlattr>.OPTIONS", "0\\1\\2");

	boost::property_tree::xml_writer_settings<std::string>settings('\t', 1); //settings for correct xml formatting
	boost::property_tree::xml_parser::write_xml(XML_NAME, xmlTree, std::locale(), settings);
}	

int Settings::getSpeedMultiplier()
{
	return stoi(settingsList[gameSpeed]);
}
int Settings::getWindWidth()
{
	return stoi(settingsList[windWidth]);
}
int Settings::getWindHeight()
{
	return stoi(settingsList[windHeight]);
}