#include "gameMenu.h"
#include "gameFlow.h"
//#include "stdafx.h"

GameMenu::GameMenu() {}

GameMenu::GameMenu(SDL_Renderer *renderer, Settings *settings) 
{
	m_rendererPtr = renderer; 
	m_gameSpeedName = getSpeedName(settings->getSpeedMultiplier());
	m_windWidth = settings->getWindWidth();
	m_windHeight = settings->getWindHeight();
	initButtons();
}

void GameMenu::initButtons() 
{
	m_startBtnPtr = std::make_unique <Button>("START GAME", m_textColorActive, m_rendererPtr, "arial.ttf", 38);
	int startBtnPosX = (m_windWidth / 2) - (m_startBtnPtr->m_buttonText->m_textRect.w / 2);
	int startBtnPosY = 150;
	int startBtnWidth = m_startBtnPtr->m_buttonText->m_textRect.w;
	int startBtnHeight = m_startBtnPtr->m_buttonText->m_textRect.h;
	m_startBtnPtr->setBtnClickArea(startBtnPosX, startBtnPosY, startBtnWidth, startBtnHeight);
	m_startBtnPtr->m_buttonText->setTextPosition(startBtnPosX, startBtnPosY);
	
	m_restartBtnPtr = std::make_unique <Button>("RESTART GAME", m_textColorInactive, m_rendererPtr, "arial.ttf", 18);
	int restartBtnPosX = (m_windWidth / 2) - (m_restartBtnPtr->m_buttonText->m_textRect.w / 2);
	int restartBtnPosY = (startBtnPosY) + (startBtnHeight) + 5;
	int restartBtnWidth = m_restartBtnPtr->m_buttonText->m_textRect.w;
	int restartBtnHeight = m_restartBtnPtr->m_buttonText->m_textRect.h;
	m_restartBtnPtr->setBtnClickArea(restartBtnPosX, restartBtnPosY, restartBtnWidth, restartBtnHeight);
	m_restartBtnPtr->m_buttonText->setTextPosition(restartBtnPosX, restartBtnPosY);
	m_restartBtnPtr->m_butState = ButtonState::inactive;
	
	m_changeSpeedBtnPtr = std::make_unique <Button>("GAME SPEED", m_textColorActive, m_rendererPtr, "arial.ttf", 34);
	int changeSpeedBtnPosX = (m_windWidth / 2) - (m_changeSpeedBtnPtr->m_buttonText->m_textRect.w / 2);
	int changeSpeedBtnPosY = (restartBtnPosY) + (restartBtnHeight) + 50;
	int changeSpeedBtnWidth = m_changeSpeedBtnPtr->m_buttonText->m_textRect.w;
	int changeSpeedBtnHeight = m_changeSpeedBtnPtr->m_buttonText->m_textRect.h;
	m_changeSpeedBtnPtr->setBtnClickArea(changeSpeedBtnPosX, changeSpeedBtnPosY, changeSpeedBtnWidth, changeSpeedBtnHeight);
	m_changeSpeedBtnPtr->m_buttonText->setTextPosition(changeSpeedBtnPosX, changeSpeedBtnPosY);
	m_changeSpeedBtnPtr->m_butState = ButtonState::active;
	
	m_speedLblPtr = std::make_unique <Label>(m_gameSpeedName, m_textColorActive, m_rendererPtr, "arial.ttf", 18); 
	int speedLblPosX = (m_windWidth / 2) - (m_speedLblPtr->m_textRect.w / 2);
	int speedLblPosY = changeSpeedBtnPosY + (changeSpeedBtnHeight) + 5;
	m_speedLblPtr->setTextPosition(speedLblPosX, speedLblPosY);

	m_exitBtnPtr = std::make_unique <Button>("EXIT GAME", m_textColorActive, m_rendererPtr, "arial.ttf", 34);
	int exitBtnPosX = (m_windWidth / 2) - (m_exitBtnPtr->m_buttonText->m_textRect.w / 2);
	int exitBtnPosY = (speedLblPosY) + (m_speedLblPtr->m_textRect.h) + 50;
	int exitBtnWidth = m_exitBtnPtr->m_buttonText->m_textRect.w;
	int exitBtnHeight = m_exitBtnPtr->m_buttonText->m_textRect.h;
	m_exitBtnPtr->setBtnClickArea(exitBtnPosX, exitBtnPosY, exitBtnWidth, exitBtnHeight);
	m_exitBtnPtr->m_buttonText->setTextPosition(exitBtnPosX, exitBtnPosY);
	
	m_chngPlayerNameLblPtr = std::make_unique <Label>("Player Name: ", m_textColorActive, m_rendererPtr, "arial.ttf", 18);
	int chngPlayerNameLblPosX = 5;
	int chngPlayerNameLblPosY = 5;
	m_chngPlayerNameLblPtr->setTextPosition(chngPlayerNameLblPosX, chngPlayerNameLblPosY);

	m_playerNameBtnPTr = std::make_unique<Button>(" " + m_playerName, m_textColorActive, m_rendererPtr, "comic.ttf", 16);
	int playerNameBtnPosX = chngPlayerNameLblPosX + m_chngPlayerNameLblPtr->m_textRect.w;
	int playerNameBtnPosY = chngPlayerNameLblPosY;
	int playerNameBtnWidth = m_playerNameBtnPTr->m_buttonText->m_textRect.w;
	int playerNameBtnHeight = m_playerNameBtnPTr->m_buttonText->m_textRect.h;
	m_playerNameBtnPTr->setBtnClickArea(playerNameBtnPosX, playerNameBtnPosY, playerNameBtnWidth, playerNameBtnHeight);
	m_playerNameBtnPTr->m_buttonText->setTextPosition(playerNameBtnPosX, playerNameBtnPosY);
	m_playerNameBtnPTr->m_butState = ButtonState::active;
	
	m_scoreListBtnPtr = std::make_unique<Button>("BEST SCORES", m_textColorActive, m_rendererPtr, "arial.ttf", 18);
	int scoreListBtnPosX = 5;
	int scoreListBtnPosY = m_windHeight - 35;
	int scoreListBtnWidth = m_scoreListBtnPtr->m_buttonText->m_textRect.w;
	int scoreListBtnHeight = m_scoreListBtnPtr->m_buttonText->m_textRect.h;
	m_scoreListBtnPtr->setBtnClickArea(scoreListBtnPosX, scoreListBtnPosY, scoreListBtnWidth, scoreListBtnHeight);
	m_scoreListBtnPtr->m_buttonText->setTextPosition(scoreListBtnPosX, scoreListBtnPosY);
	m_scoreListBtnPtr->m_butState = ButtonState::active;

	m_txtInputPromptPtr = std::make_unique<Label>("|", m_textColorActive, m_rendererPtr, "comic.ttf", 16);
	int txtInputPromptPosX = playerNameBtnPosX + playerNameBtnWidth + 3;
	int txtInputPromptPosY = playerNameBtnPosY;
	m_txtInputPromptPtr->setTextPosition(txtInputPromptPosX, txtInputPromptPosY);

	m_buttonsList.insert(make_pair(GameButton::startBtn, move(m_startBtnPtr)));
	m_buttonsList.insert(make_pair(GameButton::restartBtn, move(m_restartBtnPtr)));
	m_buttonsList.insert(make_pair(GameButton::chanSpeedBtn, move(m_changeSpeedBtnPtr)));
	m_buttonsList.insert(make_pair(GameButton::exitBtn, move(m_exitBtnPtr)));
	m_buttonsList.insert(make_pair(GameButton::playerName, move(m_playerNameBtnPTr)));
	m_buttonsList.insert(make_pair(GameButton::scoreListBtn, move(m_scoreListBtnPtr)));
}

void GameMenu::showMenu() {
	m_isMenuPresented = true;
}

void GameMenu::renderButtons() {
	for (std::unordered_map<GameButton, std::unique_ptr<Button>>:: iterator it = m_buttonsList.begin(); it != m_buttonsList.end(); ++it)
	{
		//it->second->renderBtnArea(m_rendererPtr, m_buttoncolor); // render button graphic (click area)
		it->second->renderBtnText();
	}
	m_speedLblPtr->renderText();
	m_chngPlayerNameLblPtr->renderText();
}

void GameMenu::setInputTextMenuState()
{
	m_buttonsList[GameButton::startBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
	m_buttonsList[GameButton::restartBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
	m_buttonsList[GameButton::chanSpeedBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
	m_buttonsList[GameButton::exitBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
	m_buttonsList[GameButton::scoreListBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
	m_speedLblPtr->changeTextColor(m_textColorInactive);
}
 
void GameMenu::setRegularMenuState()
{
	m_buttonsList[GameButton::startBtn]->changeBtnState(ButtonState::active, m_textColorActive);
	m_buttonsList[GameButton::restartBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
	m_buttonsList[GameButton::chanSpeedBtn]->changeBtnState(ButtonState::active, m_textColorActive);
	m_buttonsList[GameButton::exitBtn]->changeBtnState(ButtonState::active, m_textColorActive);
	m_buttonsList[GameButton::scoreListBtn]->changeBtnState(ButtonState::active, m_textColorActive);
	m_buttonsList[GameButton::playerName]->changeBtnState(ButtonState::active, m_textColorActive);
	m_speedLblPtr->changeTextColor(m_textColorActive);
	m_chngPlayerNameLblPtr->changeTextColor(m_textColorActive);
}

void GameMenu::setPausedGameMenuState()
{
	//if menu buttons are already in this state there is no need to change their state
	if (m_buttonsList[GameButton::restartBtn]->m_butState == ButtonState::inactive &&
		m_buttonsList[GameButton::chanSpeedBtn]->m_butState == ButtonState::active)
	{
		m_buttonsList[GameButton::restartBtn]->changeBtnState(ButtonState::active, m_textColorActive);
		m_buttonsList[GameButton::chanSpeedBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
		m_buttonsList[GameButton::scoreListBtn]->changeBtnState(ButtonState::inactive, m_textColorInactive);
		m_speedLblPtr->changeTextColor(m_textColorInactive);
	}
}

std::string GameMenu::getSpeedName(int speed)
{
	switch (speed)
	{
	case 0:
		return "SLOW";
		break;
	case 1:
		return "NORMAL";
		break;
	case 2:
		return "FAST";
		break;
	default:
		break;
	}
}

void GameMenu::changeSpeed(GameSpeed speed)
{
	m_speedLblPtr->changeRenderedText(getSpeedName(speed));
	m_speedLblPtr->setTextPosition((m_windWidth / 2) - (m_speedLblPtr->m_textRect.w / 2), m_speedLblPtr->m_textRect.y);
}