#include "button.h"

Button::Button(std::string text, 
				SDL_Color textColor, 
				SDL_Renderer *renderer, 
				std::string fontFile, 
				int fontSize)
{
	m_buttonText = std::make_unique<Label>(text, textColor, renderer, fontFile, fontSize);
};

Button::Button(){}

void Button::setBtnClickArea(int posX, int posY, int width, int height)
{
	m_buttonArea.x = posX;
	m_buttonArea.y = posY;
	m_buttonArea.w = width;
	m_buttonArea.h = height;
}

void Button::renderBtnText()
{
	m_buttonText->renderText();
}

void Button::renderBtnArea(SDL_Renderer *renderer, SDL_Color rectColor)
{
	SDL_SetRenderDrawColor(renderer, rectColor.r, rectColor.g, rectColor.b, 255);
	SDL_RenderFillRect(renderer, &m_buttonArea);
}

void Button::changeBtnState(ButtonState state, SDL_Color textColor)
{
	m_butState = state;
	m_buttonText->changeTextColor(textColor);
}

void Button::changeBtnText(std::string newText, bool fitBtnAreaToText)
{
	m_buttonText->changeRenderedText(newText);
	if (fitBtnAreaToText) 
	{
		int newButtonWidth = m_buttonText->m_textRect.w;
		m_buttonArea.w = newButtonWidth;
	}
}