
#include "shape.h"

Shape::~Shape(){}

void Shape::move(Direction direct)
{
	switch (direct)
	{
	case Direction::left:
		if (m_rectPositions.rect1PositionX - 1 < 0 || m_rectPositions.rect2PositionX - 1 < 0 ||
			m_rectPositions.rect3PositionX - 1 < 0 || m_rectPositions.rect4PositionX - 1 < 0)
		{
		}
		else if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX - 1, m_rectPositions.rect1PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX - 1, m_rectPositions.rect2PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX - 1, m_rectPositions.rect3PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX - 1, m_rectPositions.rect4PositionY) == FieldType::blocked)
		{
		}
		else
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);
			m_rectPositions.rect1PositionX -= 1;
			m_rectPositions.rect2PositionX -= 1;
			m_rectPositions.rect3PositionX -= 1;
			m_rectPositions.rect4PositionX -= 1;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG			
			matrixPtr->redrawMatrix();
#endif		
		}
		break;
	case Direction::right:
		if (m_rectPositions.rect1PositionX + 1 >= 16 || m_rectPositions.rect2PositionX + 1 >= 16 ||
			m_rectPositions.rect3PositionX + 1 >= 16 || m_rectPositions.rect4PositionX + 1 >= 16)
		{
		}
		else if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX + 1, m_rectPositions.rect1PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX + 1, m_rectPositions.rect2PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX + 1, m_rectPositions.rect3PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX + 1, m_rectPositions.rect4PositionY) == FieldType::blocked)
		{
		}
		else
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);
			m_rectPositions.rect1PositionX += 1;
			m_rectPositions.rect2PositionX += 1;
			m_rectPositions.rect3PositionX += 1;
			m_rectPositions.rect4PositionX += 1;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG			
			matrixPtr->redrawMatrix();
#endif
		}
		break;
	case Direction::down:
		if (m_rectPositions.rect1PositionY + 1 >= 24 || m_rectPositions.rect2PositionY + 1 >= 24 ||
			m_rectPositions.rect3PositionY + 1 >= 24 || m_rectPositions.rect4PositionY + 1 >= 24)
		{
			blockShape();
		}
		else if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX, m_rectPositions.rect1PositionY + 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX, m_rectPositions.rect2PositionY + 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX, m_rectPositions.rect3PositionY + 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX, m_rectPositions.rect4PositionY + 1) == FieldType::blocked)
		{
			blockShape();
		}
		else
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);

			m_rectPositions.rect1PositionY += 1;
			m_rectPositions.rect2PositionY += 1;
			m_rectPositions.rect3PositionY += 1;
			m_rectPositions.rect4PositionY += 1;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);

#if DEBUG		
			matrixPtr->redrawMatrix();
#endif
		}
		break;
	default:
		break;
	}
}

void Shape::blockShape()
{
	matrixPtr->fillMatrix(m_rectPositions, FieldType::blocked, shapeColor);
	m_blockedShape = true;
}

int Shape::getColorRed()
{
	return shapeColor.r;
}

int Shape::getColorGreen()
{
	return shapeColor.g;
}

int Shape::getColorBlue()
{
	return shapeColor.b;
}