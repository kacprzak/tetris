#pragma once
#include "shape.h"

class Jshape :public Shape {

public:
	Jshape(Matrix *matrix, bool m_preview);
	~Jshape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};