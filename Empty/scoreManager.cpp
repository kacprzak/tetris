#include "scoreManager.h"
using namespace std;

ScoreManager::ScoreManager(SDL_Renderer *renderer, Settings *settings)
{
	timer = make_unique<Timer>();
	timer->setStartTime();
	m_renderer = renderer;
	initScoreList();
	backgroundPosX = (settings->getWindWidth() / 2) - (backgroundWidth / 2);
	backgroundPosY = (settings->getWindHeight() / 2) - (backgroundHeight / 2);
	scoreBackgroundRect.x = backgroundPosX;
	scoreBackgroundRect.y = backgroundPosY;
	scoreBackgroundRect.w = backgroundWidth;
	scoreBackgroundRect.h = backgroundHeight;
	initScoreListLabels();
}

void ScoreManager::initScoreList()
{
	ifstream scoreFileIn("score_list.txt");
	if (scoreFileIn.is_open())
	{
		string line;
		for (unsigned i = 0; i < scoresAmount; i++)
		{
			getline(scoreFileIn, line);
			pair <string, string> playerPts = getPlayerAndScoreFromTxt(line);
			scoreList.push_back(playerPts);
		}
		scoreFileIn.close();
	}
	else
	{
		ofstream scoreFileOut("score_list.txt");
		for (unsigned i = 0; i < scoresAmount; i++)
		{
			scoreFileOut << "none-0\n";
		}
		scoreFileOut.close();

		ifstream scoreFileIn("score_list.txt");
		string line;
		for (unsigned i = 0; i < scoresAmount; i++)
		{
			getline(scoreFileIn, line);
			pair <string, string> playerPts = getPlayerAndScoreFromTxt(line);
			scoreList.push_back(playerPts);
		}
		cout << "Can't open score_list.txt file. \nNew file created. \n" << endl;
		scoreFileIn.close();
	}
}	

void ScoreManager::initScoreListLabels()
{
	scoreListHeaderPtr = make_unique <Label>("BEST SCORES", scoreFontColor, m_renderer, "arial.ttf", 28);
	scoreListHeaderPtr->setTextPosition((backgroundPosX + (backgroundWidth / 2)) - (scoreListHeaderPtr->m_textRect.w / 2), backgroundPosY + 10);
	for (unsigned i = 0; i < scoresAmount; i++)
	{
		unique_ptr <Label> scoreLblPtr = make_unique <Label>(scoreList[i].first + " - " + scoreList[i].second, scoreFontColor, m_renderer, "arial.ttf", 23);
		scoreLabelsList.push_back(move(scoreLblPtr));
	}
	scoreLabelsList[0]->setTextPosition(backgroundPosX + 10, (scoreListHeaderPtr->m_textRect.y) + (scoreListHeaderPtr->m_textRect.h) + 30);
	scoreLabelsList[1]->setTextPosition(backgroundPosX + 10, (scoreLabelsList[0]->m_textRect.y + scoreLabelsList[0]->m_textRect.h) + 25);
	scoreLabelsList[2]->setTextPosition(backgroundPosX + 10, (scoreLabelsList[1]->m_textRect.y + scoreLabelsList[1]->m_textRect.h) + 25);
	scoreLabelsList[3]->setTextPosition(backgroundPosX + 10, (scoreLabelsList[2]->m_textRect.y + scoreLabelsList[2]->m_textRect.h) + 25);
	scoreLabelsList[4]->setTextPosition(backgroundPosX + 10, (scoreLabelsList[3]->m_textRect.y + scoreLabelsList[3]->m_textRect.h) + 25);
	scoreLabelsList[5]->setTextPosition(backgroundPosX + 10, (scoreLabelsList[4]->m_textRect.y + scoreLabelsList[4]->m_textRect.h) + 25);
	continueLblPtr = make_unique <Label>("press ENTER to continue...", scoreFontColor, m_renderer, "arial.ttf", 16);
	continueLblPtr->setTextPosition((backgroundWidth / 2), backgroundPosY + backgroundHeight - 20);
}

void ScoreManager::increaseScore(int value)
{
	m_currentScore += value;
}

void ScoreManager::resetScore()
{
	m_currentScore = 0;
}

void ScoreManager::increaseTempScore(int value)
{
	m_tempScore += value;
}


void ScoreManager::drawScoreScreen()
{
	SDL_SetRenderDrawColor(m_renderer, backgroundColor.r, backgroundColor.g, backgroundColor.b, 255);
	SDL_RenderFillRect(m_renderer, &scoreBackgroundRect);
	for (unsigned i = 0; i < scoresAmount; i++)
	{
		scoreLabelsList[i]->renderText();
	}
	scoreListHeaderPtr->renderText();
	static bool showContinueLbl = true;
	timer->getTime();
	if (timer->isNextFrame(500))
	{
		showContinueLbl = !showContinueLbl;
	}
	if (showContinueLbl)
	{
		continueLblPtr->renderText();
	}
}

pair<string, string> ScoreManager::getPlayerAndScoreFromTxt(string line)
{
	pair <string, string> nameAndScore;
	string name;
	string score;
	bool delimiterReached = false;
	for (int i = 0; i < line.length(); i++)
	{
		if (line[i] == '-')
		{
			delimiterReached = true;
			continue;
		}
		if(!delimiterReached)
		{
			name += line[i];
		}
		else
		{
			score += line[i];
		}
	}
	nameAndScore.first = name;
	nameAndScore.second = score;
	return nameAndScore;
}	

void ScoreManager::checkAndSaveScore(string player, int score)
{
	bool scoreListChanged = false;
	for (vector<pair<string, string>>::iterator i = scoreList.begin(); i < scoreList.end(); i++)
	{
		if (score >= stoi((*i).second))
		{
			pair <string, string> newScore;
			newScore.first = player;
			newScore.second = to_string(score);
			scoreList.insert(i, newScore);
			scoreList.pop_back();
			scoreListChanged = true;
			break;
		}
	}
	if (scoreListChanged)
	{
		updateScoreScreen();
		ofstream scoreFileOut("score_list.txt");
		for (vector<pair<string, string>>::iterator i = scoreList.begin(); i < scoreList.end(); i++)
		{
			scoreFileOut << (*i).first << "-" << (*i).second << "\n";
		}
	}
}

void ScoreManager::updateScoreScreen() 
{
	for (unsigned i = 0; i < scoresAmount; i++)
	{
		string newLabelText = scoreList[i].first + " - " + scoreList[i].second;
		scoreLabelsList[i]->changeRenderedText(newLabelText);
	}

}