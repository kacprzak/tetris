#pragma once
#include "shape.h"

class Rshape :public Shape {

public:
	Rshape(Matrix *matrix, bool preview);
	~Rshape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};