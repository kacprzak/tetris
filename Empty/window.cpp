#include "window.h"

Window::Window(const std::string &title, Settings *settings):m_title(title)
{
	windWidth = settings->getWindWidth();
	windHeight = settings->getWindHeight();
	gameFieldWidth = 400;
	sideFieldWidth = windWidth - gameFieldWidth;
	windowClosed = !init();
	loadBackgroundImg("background.bmp");
	initBackground();
}

Window::~Window(){
	SDL_DestroyTexture(backgroundTexture);
	SDL_DestroyRenderer(m_rendererPtr);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

bool Window::init() {
	
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cerr << "Failed to initialize SDL \n";
		return 0;
	}
	m_window = SDL_CreateWindow(m_title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windWidth, windHeight, 0);
	if (m_window == nullptr) {
		std::cerr << "Failed to create Window \n";
		return 0;
	}
	m_rendererPtr = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
	if (m_rendererPtr == nullptr) {
		std::cerr << "Failed to create rengerer \n";
		return 0; 
	}
	
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
		return 0;
	}

	if (TTF_Init() == -1)
	{
		printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
		return 0;
	}
	return true;
}

bool Window::loadBackgroundImg(std::string bmpName)
{
	backgroundSurface = IMG_Load(bmpName.c_str());
	if (backgroundSurface == nullptr)
	{
		std::cout << "Can't load background image\n";
		return false;
	}
	else
	{
		return true;
	}
}

void Window::initBackground()
{
	backgroundTexture = SDL_CreateTextureFromSurface(m_rendererPtr, backgroundSurface);
	SDL_FreeSurface(backgroundSurface);
}

void Window::renderBackground()
{
	SDL_RenderCopy(m_rendererPtr, backgroundTexture, NULL, NULL);
	
}

void Window::clear() const 
{
	
	SDL_SetRenderDrawColor(m_rendererPtr, 45, 89, 134, 255);
	SDL_RenderPresent(m_rendererPtr);
	SDL_RenderClear(m_rendererPtr);
}



