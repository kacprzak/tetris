#pragma once
#include "stdafx.h"
#include "label.h"

enum class ButtonState :char { inactive, active };

struct Button 
{
	Button();
	Button(std::string text,
		SDL_Color textColor,
		SDL_Renderer *renderer,
		std::string fontFile = "arial.ttf",
		int fontSize = 24);

	SDL_Rect m_buttonArea;
	std::unique_ptr <Label> m_buttonText;
	ButtonState m_butState = ButtonState::active;
	void setBtnClickArea(int posX, int posY, int width, int height);
	void renderBtnText();
	void renderBtnArea(SDL_Renderer *renderer, SDL_Color rectColor);
	void changeBtnState(ButtonState state, SDL_Color textColor);
	void changeBtnText(std::string newText, bool fitBtnAreaToText);
};
