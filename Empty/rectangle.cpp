#include "rectangle.h"

Rectangle::Rectangle(FieldType fieldType, SDL_Color rectColor){
	m_rectType = fieldType;
	m_rectColor.r = rectColor.r;
	m_rectColor.g = rectColor.g;
	m_rectColor.b = rectColor.b;
};

Rectangle::Rectangle() {}

void Rectangle::setPosition(int posX, int posY, int width, int height) 
{
	this->x = posX;
	this->y = posY;
	this->w = width;
	this->h = height;
}

