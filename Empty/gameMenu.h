#pragma once
#include "button.h"
#include "settings.h"


enum class GameButton:char;
enum GameSpeed;
class GameFlow;

class GameMenu {

private:
	int m_windWidth;
	int m_windHeight;
	SDL_Renderer *m_rendererPtr;
	SDL_Color m_buttoncolor = { 61, 64, 68 };
	//add buttons
	std::unique_ptr <Button> m_startBtnPtr = nullptr;
	std::unique_ptr <Button> m_restartBtnPtr = nullptr;
	std::unique_ptr <Button> m_changeSpeedBtnPtr = nullptr;
	std::unique_ptr <Button> m_playerNameBtnPTr = nullptr;
	std::unique_ptr <Button> m_exitBtnPtr = nullptr;
	std::unique_ptr <Button> m_scoreListBtnPtr = nullptr;
	std::string m_gameSpeedName;
	void initButtons();
	std::string getSpeedName(int speed);

public:
	GameMenu();
	GameMenu(SDL_Renderer *renderer, Settings *settings);
	bool m_isMenuPresented = true;
	std::string m_playerName = "DEFAULT DUDE";
	//buttons text color
	SDL_Color m_textColorActive = { 255, 255, 255 };
	SDL_Color m_textColorInactive = { 170, 184, 189 };
	std::unique_ptr <Label> m_chngPlayerNameLblPtr = nullptr;
	std::unique_ptr <Label> m_speedLblPtr = nullptr;
	std::unique_ptr <Label> m_txtInputPromptPtr = nullptr;
	std::unordered_map <GameButton, std::unique_ptr<Button>> m_buttonsList;
	void showMenu();
	void renderButtons();
	void setInputTextMenuState();
	void setRegularMenuState();
	void setPausedGameMenuState();
	void changeSpeed(GameSpeed speed);
};